"""
Largest prime factor
====================

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""


def primes(limit):
    for x in range(2, limit+1):
        for y in range(2, x):
            if not x % y:
                break
        else:
            yield x


def factorize(num):
    for prime in primes(num):
        while not num % prime:
            yield prime
            num //= prime
            if num == 1:
                return


print(list(factorize(13195)))
print(list(factorize(600851475143))[-1])
