"""
Largest palindrome product
==========================

A palindromic number reads the same both ways. The largest palindrome made from
the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

from itertools import product


def products(digits):
    a = range(10 ** digits - 1, 0, -1)
    b = range(10 ** digits - 1, 0, -1)
    for x, y in product(a, b):
        yield x * y


def largest_palindrome(nums):
    result = 0
    for num in nums:
        if num > result:
            if list(str(num)) == list(reversed(str(num))):
                result = num
    return result


print(largest_palindrome(products(2)))
print(largest_palindrome(products(3)))
