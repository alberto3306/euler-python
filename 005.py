"""
Smallest multiple
=================

2520 is the smallest number that can be divided by each of the numbers from 1 to
10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the
numbers from 1 to 20?
"""

from collections import defaultdict


def least_common_multiple(nums):
    factors = defaultdict(int)
    for num in nums:
        for base, exp in factorize(num):
            factors[base] = max(factors[base], exp)
    result = 1
    for base, exp in factors.items():
        result *= base ** exp
    return result


def factorize(num):
    for prime in primes(num):
        if num == 1:
            break
        exp = 0
        while not num % prime:
            exp += 1
            num //= prime
            if num == 1:
                break
        if exp:
            yield prime, exp


def primes(limit):
    for x in range(2, limit+1):
        for y in range(2, x):
            if not x % y:
                break
        else:
            yield x


print(least_common_multiple(range(2, 11)))
print(least_common_multiple(range(2, 21)))
