"""
10001st prime
=============

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13.

What is the 10 001st prime number?
"""

from itertools import count
from math import sqrt


def nth_prime(pos):
    for n, prime in zip(range(pos), primes()):
        pass
    return prime


def primes():
    for x in count(2):
        for y in range(2, int(sqrt(x)+1)):
            if not x % y:
                break
        else:
            yield x


print(nth_prime(6))
print(nth_prime(10001))
