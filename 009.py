"""
Special Pythagorean triplet
===========================

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a2 + b2 = c2

For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""


def find_triplet(res):
    for a in range(1, res - 1):
        for b in range(1, res - a - 1):
            c = res - a - b
            assert c > 0, (a, b, c)
            assert a + b + c == res, (a, b, c)
            if a*a + b*b == c*c:
                print(a, b, c)
                return a * b * c


print(find_triplet(1000))
