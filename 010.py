"""
Summation of primes
===================

The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""

from math import sqrt


def primes_below(limit):
    result = []
    for x in range(2, limit):
        square = sqrt(x)
        for y in result:
            if y > square:
                result.append(x)
                break
            if not x % y:
                break
        else:
            result.append(x)
    return result


print(primes_below(10))
print(sum(primes_below(10)))
print(sum(primes_below(2000000)))
