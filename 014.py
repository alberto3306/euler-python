"""
Longest Collatz sequence
========================

The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even) n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains
10 terms. Although it has not been proved yet (Collatz Problem), it is thought
that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
"""

from functools import lru_cache


def collatz_num(n):
    n_even, odd = divmod(n, 2)
    n_odd = n * 3 + 1
    return n_odd if odd else n_even


@lru_cache(maxsize=None)
def collatz_seq(n):
    if n != 1:
        s = collatz_num(n)
        return (n,) + collatz_seq(s)
    return (1,)


def all_chains(limit):
    for n in range(1, limit):
        seq = collatz_seq(n)
        yield len(seq), n


def longest_chain(limit):
    return max(all_chains(limit))


print(collatz_seq(13))
print(len(collatz_seq(13)))
print(longest_chain(1000000))
