"""
Lattice paths
=============

Starting in the top left corner of a 2×2 grid, and only being able to move to
the right and down, there are exactly 6 routes to the bottom right corner.

![examples](https://projecteuler.net/project/images/p015.png)

How many such routes are there through a 20×20 grid?
"""

from functools import lru_cache


@lru_cache(maxsize=None)
def walk(side, x=0, y=0):
    if side == x == y:
        return 1
    result = 0
    if x < side:
        result += walk(side, x+1, y)
    if y < side:
        result += walk(side, x, y+1)
    return result


print(walk(2))
print(walk(20))
